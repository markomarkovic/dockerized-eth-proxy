# Dockerized [eth-proxy](https://github.com/Atrides/eth-proxy)

Edit `eth-proxy.conf` and start proxy using `docker` or `docker-compose` methods.


## Using plain `docker`

    docker build . -t eth-proxy
    docker run \
        -d \
        --name eth-proxy \
        --restart unless-stopped \
        -p 8080:8080 \
        -v `pwd`/eth-proxy.conf:/eth-proxy/eth-proxy.conf \
        -v `pwd`/log:/eth-proxy/log \
        eth-proxy


## Using `docker-compose`

    $ docker-compose up -d --build


## License

This software is provides AS-IS without any warranties of any kind. Use at your own risk.


## Donate

ETH: [0x20DCB45D699A68167712E610c22bF94dd115f207](https://www.etherchain.org/account/20DCB45D699A68167712E610c22bF94dd115f207)
