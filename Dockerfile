FROM python:2-alpine

RUN apk add --no-cache --update \
    git \
    g++ \
    && git clone https://github.com/Atrides/eth-proxy /eth-proxy \
    && rm -rf /eth-proxy/.git \
    && pip install -r /eth-proxy/requirements.txt

EXPOSE 8080

WORKDIR /eth-proxy
CMD ["python", "eth-proxy.py"]
